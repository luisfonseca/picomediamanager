mkdir -p picomediamanager/usr/bin picomediamanager/etc/pmm build/
cp pmm.py picomediamanager/usr/bin/pmm
cp config.yaml picomediamanager/etc/pmm/config.yaml
cp -r .DEBIAN/ picomediamanager/DEBIAN
dpkg-deb --build picomediamanager/
mv picomediamanager.deb build/
rm -rf picomediamanager/
