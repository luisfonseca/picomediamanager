#!/usr/bin/python3
import requests
import yaml
import re
import os, sys

regex = r"\.([0-9a-z]+)(?=[?#])|(\.)(?:[\w]+)$"

f = open('/etc/pmm/config.yaml', 'r')
conf = yaml.load(f)
f.close()

path = os.path.realpath(sys.argv[1]) if len(sys.argv) > 1 else os.getcwd()

separator = r'='*55+' '

def choose(choices_, default):
    choices = choices_.copy() 
    if default >= len(choices):
        default = 0

    for i in range(len(choices)):
        print( str(i) + ') ' + choices[i] + (' *' if i==default else ''))
    
    choice = input(separator)
    if choice == '':
        return default
    else:
        return int(choice)

def select(choices_, selected):
    choices = choices_.copy() 
    choices.insert(0,'Toggle All')

    for i in range(len(choices)):
        print( ('* ' if {i-1}.issubset(selected) else '  ') + str(i) + ') ' + choices[i])
    
    choice = input(separator)
    if choice == '':
        selection = []
        for i in range(len(choices)):
            if {i-1}.issubset(selected):
                selection.append(choices[i])

        return selection

    else:
        sel = set(())
        for i in choice.split(','):
            if i == '0':
                selected.symmetric_difference_update({*range(len(choices))})
            else:
                sel.add(int(i)-1)

        return select(choices[1:],selected.symmetric_difference(sel))


def ask(question,default):

    ans=input(question + (' (' + default + '): ' if default != '' else ' '))
    if ans == '':
        return default if default != '' else ask(question,default)
    else:
        return ans

def main():
    print(
'''
     ____  _             __  __          _ _       
    |  _ \(_) ___ ___   |  \/  | ___  __| (_) __ _ 
    | |_) | |/ __/ _ \  | |\/| |/ _ \/ _` | |/ _` |
    |  __/| | (_| (_) | | |  | |  __/ (_| | | (_| |
    |_|   |_|\___\___/  |_|  |_|\___|\__,_|_|\__,_|
                                               
       __  __                                   
      |  \/  | __ _ _ __   __ _  __ _  ___ _ __ 
      | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
      | |  | | (_| | | | | (_| | (_| |  __/ |   
      |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|   
                                |___/           
'''
    )
    print(separator)
    print(separator)
    get_files()

def get_files():
    if os.path.isfile(path):
        files = [path]
    elif os.path.isdir(path):
        files = sorted(os.listdir(path))
    else:
        raise Exception('Invalid Path')
    
    print('Select the files you wish to process.')
    
    files = select(files,{*range(len(files))})
    
    if len(files) > 1:
        search(files,'tv')
    elif len(files) == 1:
        choose_media(files)
    else:
        print('Nothing was selected')
        get_files()


def choose_media(files):
    print('Is it a Movie or a TV Series Episode?')
    media = ['movie','tv'][choose(['Movie','TV'],0)]
    search(files,media)

def search(files,media):
    url = 'https://api.themoviedb.org/3/search/' + media
    params = dict.fromkeys(('api_key','query'))

    params['api_key'] = conf['api_key']
    params['query'] = ask('Name?','')

    r = requests.get(url,params)
    results = r.json()['results']

    names=[]
    cids=[]
    for result in results:
        name = result[('name' if media=='tv' else 'title')]
        try:
            date = ' (' + result[('first_air_date' if media=='tv' else 'release_date')][:4] + ')'
            name += date if date != ' ()' else ''
        except Exception:
            pass
        names.append(name)
        cids.append(result['id'])

    names.insert(0, 'None')
    print('Which one of these?')
    i = choose(names,1)
    if i==0:
        print('What is wrong?')
        if choose(['Wrong title','Not '+media],0) == 0:
            search(files,media)
        else:
            choose_media(files)

    else:
        (tv if media=='tv' else movie)(files,cids[i-1])
        
def movie(files,cid):

    r = requests.get('https://api.themoviedb.org/3/movie/'+str(cid),dict(api_key=conf['api_key']))
    info = r.json()
    name = info['title']+ (' (' + info['release_date'][:4] + ')' if info['release_date'] is not None else '')

    extension = re.search(regex, files[0]).group()

    src = [path + '/' + files[0]]
    dest = [os.path.realpath(conf['movie_dir']) + '/' + name + '/' + name + extension]

    media_pairs(src,dest)

def tv(files,cid):
    r = requests.get('https://api.themoviedb.org/3/tv/'+str(cid),dict(api_key=conf['api_key']))
    info = r.json()
    name = info['name']+ (' (' + info['first_air_date'][:4] + ')' if info['first_air_date'] is not None else '')
    
    season_names = []
    for season in info['seasons']:
        season_names.append(season['name'])
    
    season = choose(season_names,0)

    if season == len(info['seasons'])-1 and info['last_episode_to_air']['season_number'] == info['seasons'][season]['season_number']:
        ep_count = info['last_episode_to_air']['episode_number']
    else:
        ep_count = info['seasons'][season]['episode_count']

    episodes = []
    season_nr = info['seasons'][season]['season_number']
    for ep in range(ep_count):
        episodes.append('S' + f'{season_nr:02}' + 'E' + f'{ep+1:02}')
    
    while True:
        print('Select the episodes the media files correspond too.')

        sel = select(episodes,{*range(ep_count)})
        if len(sel) == len(files):
            episodes = sel
            break
        else:
            print('You need to select ' + str(len(files)) + ' episodes.')

    src = []
    dest = []
    for i in range(len(episodes)):
        extension = re.search(regex, files[i]).group()
        src.append(path + '/' + files[i])
        dest.append(os.path.realpath(conf['tv_dir']) + '/' + name + '/' + name + ' ' + episodes[i] + extension)

    media_pairs(src,dest)

def media_pairs(src,dest):
    if len(src) != len(dest):
        raise Exception('there are different amounts of source and destination files')
    
    cnt = len(src)

    for i in range(cnt):
        if os.path.isfile(dest[i]):
            if os.path.islink(dest[i]):
                os.remove(dest[i])
                print('A symlink for ' + dest[i] + ' was removed!')
            else:
                print(dest[i] + ' is already a file, this script won\'t overwrite any regular file so please manually remove the file')
                sys.exit()

    
    os.makedirs('/'.join(dest[0].split('/')[:-1])+'/',exist_ok=True)
    for i in range(cnt):
        os.symlink(src[i],dest[i])
        print(src[i] + ' has been linked to ' + dest[i])

if __name__ == '__main__':
    main()
