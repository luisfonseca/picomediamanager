# Pico Media Manager

PMM is a tiny TMDB based CLI Mass Renamer.

It will symlink media files from the specified directory to the media folders defined in the configuration.
This way, you may keep seeding your legally torrented tv shows but hide the release group tags and folder structure mess.
The files are organized according to [KODI's guidelines](https://kodi.wiki/view/Naming_video_files) making it media center friendly.

## Installation

Build the debian package and install it with apt-get

```bash
./build.sh
sudo apt-get install build/picomediamanager.deb
```

A destribution agnostic aproach will require changing the location of config.yaml in pmm.py and adding pmm.py to path.
You'll need to install the modules requests and pyyaml

```python
regex = r"\.([0-9a-z]+)(?=[?#])|(\.)(?:[\w]+)$"

f.open('/full/path/to/config.yaml', 'r')
conf = yaml.load(f)
f.close()
```

## Configuration

Edit config.yaml:

```yaml
api_key: <Replace with your API key for TMDB>
movie_dir: <Your movies will be stored in this folder>
tv_dir: <Your tv shows will be stored in this folder>
```

## Usage

Run the command and follow the interactive prompt.
When no argument is given it will use the current working directory.
```bash
pmm <path_to_folder_cointaining_media>
```
(Use pmm.py instead of pmm if you added it to path manually)

## Example

```
sysop@Polaris:~$ pmm Downloads/Neon\ Genesis\ Evangelion\ 1-26\ DC+EoE\ \[MULTI\]\[BD\ 1080p\ 8bits\ v2.22\]\[Sephirotic\]/

     ____  _             __  __          _ _
    |  _ \(_) ___ ___   |  \/  | ___  __| (_) __ _
    | |_) | |/ __/ _ \  | |\/| |/ _ \/ _` | |/ _` |
    |  __/| | (_| (_) | | |  | |  __/ (_| | | (_| |
    |_|   |_|\___\___/  |_|  |_|\___|\__,_|_|\__,_|

       __  __
      |  \/  | __ _ _ __   __ _  __ _  ___ _ __
      | |\/| |/ _` | '_ \ / _` |/ _` |/ _ \ '__|
      | |  | | (_| | | | | (_| | (_| |  __/ |
      |_|  |_|\__,_|_| |_|\__,_|\__, |\___|_|
                                |___/

=======================================================
=======================================================
Select the files you wish to process.
  0) Toggle All
* 1) READMEv2.22.txt
* 2) The End of Evangelion - Notas de Tradução v1.1.doc
* 3) The End of Evangelion - Translation notes v1.4.doc
* 4) [Sephirotic] Evangelion - 01 [MULTI][BD 1080p 8bits 5.1 AAC][6B1607E9].mkv
* 5) [Sephirotic] Evangelion - 02 [MULTI][BD 1080p 8bits 5.1 AAC][C08EC897].mkv
* 6) [Sephirotic] Evangelion - 03 [MULTI][BD 1080p 8bits 5.1 AAC][7C6069A9].mkv
* 7) [Sephirotic] Evangelion - 04 [MULTI][BD 1080p 8bits 5.1 AAC][ECEA0327].mkv
* 8) [Sephirotic] Evangelion - 05 [MULTI][BD 1080p 8bits 5.1 AAC][44CF9DD2].mkv
* 9) [Sephirotic] Evangelion - 06 [MULTI][BD 1080p 8bits 5.1 AAC][B8663473].mkv
* 10) [Sephirotic] Evangelion - 07 [MULTI][BD 1080p 8bits 5.1 AAC][3FAF8818].mkv
* 11) [Sephirotic] Evangelion - 08 [MULTI][BD 1080p 8bits 5.1 AAC][ED1E605A].mkv
* 12) [Sephirotic] Evangelion - 09 [MULTI][BD 1080p 8bits 5.1 AAC][73B322AB].mkv
* 13) [Sephirotic] Evangelion - 10 [MULTI][BD 1080p 8bits 5.1 AAC][B7A0A8A0].mkv
* 14) [Sephirotic] Evangelion - 11 [MULTI][BD 1080p 8bits 5.1 AAC][3A368205].mkv
* 15) [Sephirotic] Evangelion - 12 [MULTI][BD 1080p 8bits 5.1 AAC][D31DD6E2].mkv
* 16) [Sephirotic] Evangelion - 13 [MULTI][BD 1080p 8bits 5.1 AAC][E4AD143C].mkv
* 17) [Sephirotic] Evangelion - 14 [MULTI][BD 1080p 8bits 5.1 AAC][DACAAD89].mkv
* 18) [Sephirotic] Evangelion - 15 [MULTI][BD 1080p 8bits 5.1 AAC][FB7C166E].mkv
* 19) [Sephirotic] Evangelion - 16 [MULTI][BD 1080p 8bits 5.1 AAC][746F7053].mkv
* 20) [Sephirotic] Evangelion - 17 [MULTI][BD 1080p 8bits 5.1 AAC][68C48B9D].mkv
* 21) [Sephirotic] Evangelion - 18 [MULTI][BD 1080p 8bits 5.1 AAC][C07227AA].mkv
* 22) [Sephirotic] Evangelion - 19 [MULTI][BD 1080p 8bits 5.1 AAC][FE27EA55].mkv
* 23) [Sephirotic] Evangelion - 20 [MULTI][BD 1080p 8bits 5.1 AAC][E7D2052F].mkv
* 24) [Sephirotic] Evangelion - 21' [MULTI][BD 1080p 8bits 5.1 AAC][D128B2D3].mkv
* 25) [Sephirotic] Evangelion - 22' [MULTI][BD 1080p 8bits 5.1 AAC][82842B04].mkv
* 26) [Sephirotic] Evangelion - 23' [MULTI][BD 1080p 8bits 5.1 AAC][78ADF63D].mkv
* 27) [Sephirotic] Evangelion - 24' [MULTI][BD 1080p 8bits 5.1 AAC][25378AB4].mkv
* 28) [Sephirotic] Evangelion - 25 [MULTI][BD 1080p 8bits 5.1 AAC][F5AC978E].mkv
* 29) [Sephirotic] Evangelion - 26 [MULTI][BD 1080p 8bits 5.1 AAC][B0C23B03].mkv
* 30) [Sephirotic] Evangelion - The End of Evangelion - 25' [MULTI][BD 1080p 8bits 5.1 AAC][80C45029].mkv
* 31) [Sephirotic] Evangelion - The End of Evangelion - 26' [MULTI][BD 1080p 8bits 5.1 AAC][84BEB803].mkv
* 32) [Sephirotic] Evangelion - The End of Evangelion 25'+26' (Conjoined Ordered Chapter)[C6FDE4FB].mkv
======================================================= 1,2
  0) Toggle All
  1) READMEv2.22.txt
  2) The End of Evangelion - Notas de Tradução v1.1.doc
* 3) The End of Evangelion - Translation notes v1.4.doc
* 4) [Sephirotic] Evangelion - 01 [MULTI][BD 1080p 8bits 5.1 AAC][6B1607E9].mkv
* 5) [Sephirotic] Evangelion - 02 [MULTI][BD 1080p 8bits 5.1 AAC][C08EC897].mkv
* 6) [Sephirotic] Evangelion - 03 [MULTI][BD 1080p 8bits 5.1 AAC][7C6069A9].mkv
* 7) [Sephirotic] Evangelion - 04 [MULTI][BD 1080p 8bits 5.1 AAC][ECEA0327].mkv
* 8) [Sephirotic] Evangelion - 05 [MULTI][BD 1080p 8bits 5.1 AAC][44CF9DD2].mkv
* 9) [Sephirotic] Evangelion - 06 [MULTI][BD 1080p 8bits 5.1 AAC][B8663473].mkv
* 10) [Sephirotic] Evangelion - 07 [MULTI][BD 1080p 8bits 5.1 AAC][3FAF8818].mkv
* 11) [Sephirotic] Evangelion - 08 [MULTI][BD 1080p 8bits 5.1 AAC][ED1E605A].mkv
* 12) [Sephirotic] Evangelion - 09 [MULTI][BD 1080p 8bits 5.1 AAC][73B322AB].mkv
* 13) [Sephirotic] Evangelion - 10 [MULTI][BD 1080p 8bits 5.1 AAC][B7A0A8A0].mkv
* 14) [Sephirotic] Evangelion - 11 [MULTI][BD 1080p 8bits 5.1 AAC][3A368205].mkv
* 15) [Sephirotic] Evangelion - 12 [MULTI][BD 1080p 8bits 5.1 AAC][D31DD6E2].mkv
* 16) [Sephirotic] Evangelion - 13 [MULTI][BD 1080p 8bits 5.1 AAC][E4AD143C].mkv
* 17) [Sephirotic] Evangelion - 14 [MULTI][BD 1080p 8bits 5.1 AAC][DACAAD89].mkv
* 18) [Sephirotic] Evangelion - 15 [MULTI][BD 1080p 8bits 5.1 AAC][FB7C166E].mkv
* 19) [Sephirotic] Evangelion - 16 [MULTI][BD 1080p 8bits 5.1 AAC][746F7053].mkv
* 20) [Sephirotic] Evangelion - 17 [MULTI][BD 1080p 8bits 5.1 AAC][68C48B9D].mkv
* 21) [Sephirotic] Evangelion - 18 [MULTI][BD 1080p 8bits 5.1 AAC][C07227AA].mkv
* 22) [Sephirotic] Evangelion - 19 [MULTI][BD 1080p 8bits 5.1 AAC][FE27EA55].mkv
* 23) [Sephirotic] Evangelion - 20 [MULTI][BD 1080p 8bits 5.1 AAC][E7D2052F].mkv
* 24) [Sephirotic] Evangelion - 21' [MULTI][BD 1080p 8bits 5.1 AAC][D128B2D3].mkv
* 25) [Sephirotic] Evangelion - 22' [MULTI][BD 1080p 8bits 5.1 AAC][82842B04].mkv
* 26) [Sephirotic] Evangelion - 23' [MULTI][BD 1080p 8bits 5.1 AAC][78ADF63D].mkv
* 27) [Sephirotic] Evangelion - 24' [MULTI][BD 1080p 8bits 5.1 AAC][25378AB4].mkv
* 28) [Sephirotic] Evangelion - 25 [MULTI][BD 1080p 8bits 5.1 AAC][F5AC978E].mkv
* 29) [Sephirotic] Evangelion - 26 [MULTI][BD 1080p 8bits 5.1 AAC][B0C23B03].mkv
* 30) [Sephirotic] Evangelion - The End of Evangelion - 25' [MULTI][BD 1080p 8bits 5.1 AAC][80C45029].mkv
* 31) [Sephirotic] Evangelion - The End of Evangelion - 26' [MULTI][BD 1080p 8bits 5.1 AAC][84BEB803].mkv
* 32) [Sephirotic] Evangelion - The End of Evangelion 25'+26' (Conjoined Ordered Chapter)[C6FDE4FB].mkv
======================================================= 3,30,31,32
  0) Toggle All
  1) READMEv2.22.txt
  2) The End of Evangelion - Notas de Tradução v1.1.doc
  3) The End of Evangelion - Translation notes v1.4.doc
* 4) [Sephirotic] Evangelion - 01 [MULTI][BD 1080p 8bits 5.1 AAC][6B1607E9].mkv
* 5) [Sephirotic] Evangelion - 02 [MULTI][BD 1080p 8bits 5.1 AAC][C08EC897].mkv
* 6) [Sephirotic] Evangelion - 03 [MULTI][BD 1080p 8bits 5.1 AAC][7C6069A9].mkv
* 7) [Sephirotic] Evangelion - 04 [MULTI][BD 1080p 8bits 5.1 AAC][ECEA0327].mkv
* 8) [Sephirotic] Evangelion - 05 [MULTI][BD 1080p 8bits 5.1 AAC][44CF9DD2].mkv
* 9) [Sephirotic] Evangelion - 06 [MULTI][BD 1080p 8bits 5.1 AAC][B8663473].mkv
* 10) [Sephirotic] Evangelion - 07 [MULTI][BD 1080p 8bits 5.1 AAC][3FAF8818].mkv
* 11) [Sephirotic] Evangelion - 08 [MULTI][BD 1080p 8bits 5.1 AAC][ED1E605A].mkv
* 12) [Sephirotic] Evangelion - 09 [MULTI][BD 1080p 8bits 5.1 AAC][73B322AB].mkv
* 13) [Sephirotic] Evangelion - 10 [MULTI][BD 1080p 8bits 5.1 AAC][B7A0A8A0].mkv
* 14) [Sephirotic] Evangelion - 11 [MULTI][BD 1080p 8bits 5.1 AAC][3A368205].mkv
* 15) [Sephirotic] Evangelion - 12 [MULTI][BD 1080p 8bits 5.1 AAC][D31DD6E2].mkv
* 16) [Sephirotic] Evangelion - 13 [MULTI][BD 1080p 8bits 5.1 AAC][E4AD143C].mkv
* 17) [Sephirotic] Evangelion - 14 [MULTI][BD 1080p 8bits 5.1 AAC][DACAAD89].mkv
* 18) [Sephirotic] Evangelion - 15 [MULTI][BD 1080p 8bits 5.1 AAC][FB7C166E].mkv
* 19) [Sephirotic] Evangelion - 16 [MULTI][BD 1080p 8bits 5.1 AAC][746F7053].mkv
* 20) [Sephirotic] Evangelion - 17 [MULTI][BD 1080p 8bits 5.1 AAC][68C48B9D].mkv
* 21) [Sephirotic] Evangelion - 18 [MULTI][BD 1080p 8bits 5.1 AAC][C07227AA].mkv
* 22) [Sephirotic] Evangelion - 19 [MULTI][BD 1080p 8bits 5.1 AAC][FE27EA55].mkv
* 23) [Sephirotic] Evangelion - 20 [MULTI][BD 1080p 8bits 5.1 AAC][E7D2052F].mkv
* 24) [Sephirotic] Evangelion - 21' [MULTI][BD 1080p 8bits 5.1 AAC][D128B2D3].mkv
* 25) [Sephirotic] Evangelion - 22' [MULTI][BD 1080p 8bits 5.1 AAC][82842B04].mkv
* 26) [Sephirotic] Evangelion - 23' [MULTI][BD 1080p 8bits 5.1 AAC][78ADF63D].mkv
* 27) [Sephirotic] Evangelion - 24' [MULTI][BD 1080p 8bits 5.1 AAC][25378AB4].mkv
* 28) [Sephirotic] Evangelion - 25 [MULTI][BD 1080p 8bits 5.1 AAC][F5AC978E].mkv
* 29) [Sephirotic] Evangelion - 26 [MULTI][BD 1080p 8bits 5.1 AAC][B0C23B03].mkv
  30) [Sephirotic] Evangelion - The End of Evangelion - 25' [MULTI][BD 1080p 8bits 5.1 AAC][80C45029].mkv
  31) [Sephirotic] Evangelion - The End of Evangelion - 26' [MULTI][BD 1080p 8bits 5.1 AAC][84BEB803].mkv
  32) [Sephirotic] Evangelion - The End of Evangelion 25'+26' (Conjoined Ordered Chapter)[C6FDE4FB].mkv
=======================================================
Name? Evangelion
Which one of these?
0) None
1) Neon Genesis Evangelion (1995) *
2) Petit Eva: Evangelion@School (2007)
=======================================================
0) Season 1 *
=======================================================
Select the episodes the media files correspond too.
  0) Toggle All
* 1) S01E01
* 2) S01E02
* 3) S01E03
* 4) S01E04
* 5) S01E05
* 6) S01E06
* 7) S01E07
* 8) S01E08
* 9) S01E09
* 10) S01E10
* 11) S01E11
* 12) S01E12
* 13) S01E13
* 14) S01E14
* 15) S01E15
* 16) S01E16
* 17) S01E17
* 18) S01E18
* 19) S01E19
* 20) S01E20
* 21) S01E21
* 22) S01E22
* 23) S01E23
* 24) S01E24
* 25) S01E25
* 26) S01E26
=======================================================
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 01 [MULTI][BD 1080p 8bits 5.1 AAC][6B1607E9].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E01.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 02 [MULTI][BD 1080p 8bits 5.1 AAC][C08EC897].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E02.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 03 [MULTI][BD 1080p 8bits 5.1 AAC][7C6069A9].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E03.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 04 [MULTI][BD 1080p 8bits 5.1 AAC][ECEA0327].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E04.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 05 [MULTI][BD 1080p 8bits 5.1 AAC][44CF9DD2].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E05.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 06 [MULTI][BD 1080p 8bits 5.1 AAC][B8663473].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E06.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 07 [MULTI][BD 1080p 8bits 5.1 AAC][3FAF8818].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E07.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 08 [MULTI][BD 1080p 8bits 5.1 AAC][ED1E605A].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E08.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 09 [MULTI][BD 1080p 8bits 5.1 AAC][73B322AB].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E09.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 10 [MULTI][BD 1080p 8bits 5.1 AAC][B7A0A8A0].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E10.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 11 [MULTI][BD 1080p 8bits 5.1 AAC][3A368205].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E11.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 12 [MULTI][BD 1080p 8bits 5.1 AAC][D31DD6E2].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E12.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 13 [MULTI][BD 1080p 8bits 5.1 AAC][E4AD143C].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E13.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 14 [MULTI][BD 1080p 8bits 5.1 AAC][DACAAD89].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E14.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 15 [MULTI][BD 1080p 8bits 5.1 AAC][FB7C166E].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E15.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 16 [MULTI][BD 1080p 8bits 5.1 AAC][746F7053].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E16.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 17 [MULTI][BD 1080p 8bits 5.1 AAC][68C48B9D].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E17.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 18 [MULTI][BD 1080p 8bits 5.1 AAC][C07227AA].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E18.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 19 [MULTI][BD 1080p 8bits 5.1 AAC][FE27EA55].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E19.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 20 [MULTI][BD 1080p 8bits 5.1 AAC][E7D2052F].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E20.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 21' [MULTI][BD 1080p 8bits 5.1 AAC][D128B2D3].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E21.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 22' [MULTI][BD 1080p 8bits 5.1 AAC][82842B04].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E22.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 23' [MULTI][BD 1080p 8bits 5.1 AAC][78ADF63D].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E23.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 24' [MULTI][BD 1080p 8bits 5.1 AAC][25378AB4].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E24.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 25 [MULTI][BD 1080p 8bits 5.1 AAC][F5AC978E].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E25.mkv
/home/sysop/Downloads/Neon Genesis Evangelion 1-26 DC+EoE [MULTI][BD 1080p 8bits v2.22][Sephirotic]/[Sephirotic] Evangelion - 26 [MULTI][BD 1080p 8bits 5.1 AAC][B0C23B03].mkv has been linked to /srv/Media/TV/Neon Genesis Evangelion (1995)/Neon Genesis Evangelion (1995) S01E26.mkv
```

## License
[UNLICENSE](https://unlicense.org/)